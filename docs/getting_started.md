# Getting Started

## Settings

Moved to [settings](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

## Basic Commands

### Running locally with Docker

Open a terminal at the project root and run the following for local
development:

```bash
make up
```

The web application is accessible at <http://localhost:8000>.

For the first time you will need to run migrations with:

```bash
make dev.migrate
```

Optionally, You can also set the environment variable `COMPOSE_FILE` pointing to `local.yml` like this:

```bash
export COMPOSE_FILE=local.yml
```

Please see cookiecutter-django docs for more information about running locally
[with Docker](https://cookiecutter-django.readthedocs.io/en/latest/developing-locally-docker.html).

### Setting Up Your Users

- To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it,
  you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email
  verification message. Copy the link into your browser. Now the user's email should be verified
  and ready to go.
- To create a **superuser account**, use this command:

  ```bash
  make dev.manage.createsuperuser
  ```

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on
Firefox (or similar), so that you can see how the site behaves for both kinds of users.

### Type checks

Running type checks with mypy:

```bash
make dev.test.quality
```

#### Running unit tests with pytest

```bash
make test.unit
```

### Generate coverage report in HTML

After running unit tests, you can generate an HTML coverage report:

```bash
make dev.cov.html
```

The results will be available in the `htmlcov/index.html`. You can open it with your browser.

### Celery

This app comes with Celery. To run a celery worker, use:

```bash
docker-compose -f local.yml run django celery -A config.celery_app worker -l info
```

Please note: For Celery's import magic to work, it is important *where* the celery commands are run.
If you are in the same folder with *manage.py*, you should be right.

## Deployment

Sprintcraft is running on Kubernetes and Cloudflare Pages. The backend is deployed using [Helm Charts],
though chart packaging and deployment is not automated yet.

Creating tags on GitLab is mandatory to be able to package helm charts and deploy the frontend.

### Helm Chart

The Helm Chart is located in the [Helm Charts] repository. For packaging and deployment instructions,
please refer to the README in the repository.

[Helm Charts]: https://gitlab.com/opencraft/ops/helm-charts/-/tree/main/charts/sprintcraft

### Frontend

The frontend is deployed using Cloudflare Pages. It is important that frontend is not deployed upon
merging to master. It will be deployed when someone creates a new git tag and pushes it to GitLab.
Therefore, we have control over the timings -- what to deploy and when.
