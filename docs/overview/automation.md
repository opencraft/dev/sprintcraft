# Automation

SprintCraft implement tasks that automate some parts of the sprint planning process.
To enable automation, set the `FEATURE_SPRINT_AUTOMATION` env variable to `True`.

## Pinging people

The automations retrieve users responsible for a ticket. The following rules apply for this:

1. The assignee is included if the ticket is assigned.
2. The epic owner is included if the ticket is unassigned or if a task explicitly requests this.
3. The reporter is included if the ticket both:
    1. Is unassigned.
    2. Does not belong to an epic or the epic is unassigned.
4. If none of the above is present, the error is reported to Sentry. A task determines whether
   the users will be pinged on the ticket (with an asynchronous comment) or via the Mattermost
   (with a synchronous message), depending on the urgency of this part of the sprint planning process.

## Scheduling tasks

While completing the sprint, the automation tasks are scheduled for the new one. There are two types
of supported tasks: 1. One-off - ran on a specific day of the sprint. 2. Periodic - ran hourly from
a specific day of the sprint to either another day or until the end of the sprint.

You can see the scheduled tickets in the Django admin panel
(<http://localhost:8000/admin/django_celery_beat/periodictask/>).

## Ticket planning

These tasks relate to planning the tickets for the next sprint.

### Handle task injections

To make the sprint planning easier, we have introduced a ticket creation cutoff day. From this day
of the sprint, it is no longer possible to add tickets to the next sprint. If the ticket needs to be
added to the next sprint, then it's added to "Stretch Goals", and then it's picked up only if the cell
has the capacity, as described in the [Task Insertion][task-insertion] section of our handbook.

If a ticket is added to the next sprint after the cutoff day, it will be automatically moved to the
\"Stretch Goals\" sprint, then the ticket's reporter and the epic owner will be notified about this
via a comment on the ticket. To accept a sprint injection, a specific label (`injection-accepted`
by default) needs to be added to the ticket by the [Sprint Planning Manager][sprint-planning-manager].

This is a periodic task, which is running hourly from the cutoff day until the end of the sprint.

### Check if all tasks are ready for the next sprint

This task determines whether all tickets have the following attributes set:

1. Assignee.
2. Reviewer.
3. Story Points.

Each person, who has some incomplete tickets, will be pinged on Mattermost, with a list of these
tickets, with sub-lists of their missing fields.

This is a one-off task, which runs at the beginning of the sprint's final day.

### Find overcommitted people

This task uses Mattermost to ping people who have negative time left for the next sprint
(i.e. are overcommitted).

This is a one-off task, which runs at the beginning of the sprint's final day.

## Estimation session

For estimating tickets, we are using the [Agile Poker][agile-poker] Jira app.

### Creating sessions

At the beginning of the sprint, a new session is created for each cell.

Creating a session without issues causes some chaos in Jira, as the `/session/async/{sessionId}/rounds/`
endpoint returns HTTP 500 in such case. It does not break other API calls, so operations like updating,
closing, and deleting the session (via the API) work correctly. It makes the session unusable via the
browser by breaking two views: - estimation, - configuration. Therefore, the decision is to avoid adding
the participants to the session until there are issues that can be added too. Assuming that the sessions
are fully automated, and don't require any manual interventions in the beginning, this should not
cause any troubles.

This is a one-off task, which runs at the beginning of the sprint. The email notification is not sent,
because there are no participants.

### Send Reminder emails

After the ticket creating cut-off date, a reminder email is sent to each cell with the link
to participate in estimation session.

### Updating sessions

This task adds any tickets that have been added to the next sprint but are not present in the estimation
session. It also adds participants, when there are tickets scheduled for the next sprint (please see the
explanation above), or if a new member joins a cell.

This does not override the manual additions to the session - i.e. if a ticket or user has been added
manually to the session, then it will be retained, as it merges available issues and participants with the
applied ones. However, any removed items (e.g. ticket scheduled for the next sprint or of a user,
who is a member of the cell) will be added back automatically.

This is a periodic task, which is running hourly from the beginning of the sprint until the final day
of the sprint. The participants are notified about each change via email, so they are aware of the
unestimated tickets.

### Closing sessions

The session is closed for each cell before the sprint's final day. This
triggers the [Moving estimates to tickets](#moving-estimates-to-tickets) task.

This is a one-off task, which runs at the beginning of the sprint. The
participants are notified about this via email.

### Moving estimates to tickets

This applies the average vote results from the closed estimation session
to all tickets. In the case of a draw, the higher estimate is returned.

If there were no votes for a specific ticket, its assignee (or another responsible person) is notified.

### Configuration variables

Please see the [configurations][configurations] for a detailed description of these variables.

1. `FEATURE_SPRINT_AUTOMATION`
2. `SPRINT_ASYNC_TICKET_CREATION_CUTOFF_DAY`
3. `SPRINT_ASYNC_INJECTION_LABEL`
4. `SPRINT_ASYNC_INJECTION_SPRINT`
5. `SPRINT_ASYNC_INJECTION_MESSAGE`
6. `SPRINT_ASYNC_TICKET_FINAL_CHECK_DAY`
7. `SPRINT_ASYNC_POKER_NEW_SESSION_MESSAGE`
8. `SPRINT_ASYNC_POKER_NO_ESTIMATES_MESSAGE`
9. `SPRINT_ASYNC_INCOMPLETE_TICKET_MESSAGE`
10. `SPRINT_ASYNC_OVERCOMMITMENT_MESSAGE`

## Sprint completion

This task triggers sprint completion for every cell. It can be enabled by setting
the `FEATURE_AUTOMATED_SPRINT_COMPLETION` env variable to `True`.

This is a one-off task, which runs at the end of the sprint, at midnight.

<!-- LINKS -->

[configurations]: https://gitlab.com/opencraft/dev/sprintcraft/-/blob/master/config/settings/base.py
[task-insertion]: https://handbook.opencraft.com/en/latest/processes/sprints/#task-insertion
[sprint-planning-manager]: https://handbook.opencraft.com/en/latest/roles/list/#sprint-planning-manager
[agile-poker]: https://marketplace.atlassian.com/apps/700473/agile-poker-for-jira-planning-estimation
