# Sustainability

The Sustainability Dashboard and Budget Dashboard (both described below) are aware of the sprint
board's current view (i.e, whether the sprint board is currently showing a list of cells,
a specific cell's commitments, or a team member's commitments).

Therefore, when you click on the name of a cell (or the name of a team member),
the sustainability dashboard recalculates its statistics for displaying cell-specific (person-specific)
data only.

## Sustainability Targets

Sustainability targets for individual cells and the company as a whole can be configured from the SprintCraft admin.
See [Setting up Sustainability Targets](#setting-up-sustainability-targets) for details.

💡 As [mentioned](https://handbook.opencraft.com/en/latest/cells/budgets/#cell-and-company-wide-sustainability) in the handbook,
sustainability targets for individual cells depend on their type (project vs generalist vs support), focus area(s),
and current set of clients and projects.

## Sustainability Dashboard

This dashboard allows you to check sustainability status at the company, cell, and user level.

At each level, the key information is the *sustainability ratio*, i.e., the ratio of
non-billable hours to total hours logged for the period under review:

`% non-billable = non-billable hours / total hours`

A cell (or the company as a whole) is considered sustainable for the period under review
if its sustainability ratio does not exceed the cell's/company's current [sustainability target](#sustainability-targets).

### Company-wide sustainability

At the highest level we can view the company's sustainability status which takes into account time logged
by all team members over the the course of the period under review, regardless of the cells that they belong to.

The sustainability dashboard lists:

#### Total hours

Billable hours + non-billable hours

#### Billable hours

Number of hours logged against billable accounts that are covered by existing budgets (i.e., hours that do *not* count as overhead).

#### Non-billable hours

Sum of:

* Hours logged against billable accounts that are *not* covered by existing budgets (i.e., hours that count as overhead).
* Hours logged against non-billable accounts.

#### Ratio of non-billable hours

Non-billable hours / total hours

### Cell-specific sustainability

At this level we can view the sustainability status for a specific cell (= *project* in Tempo terms).

The sustainability dashboard lists:

#### Total hours (cell)

Billable hours + non-billable hours logged against tickets belonging to the current cell (e.g. BB, FAL, SE).

#### Billable hours (cell)

Billable hours logged against tickets belonging to the current cell (e.g. BB, FAL, SE).

Hours are considered billable if they are:

1. Logged against billable account.
1. Covered by existing budgets (otherwise they count as overhead and will be added to the total number of non-billable hours).

#### Non-billable hours (cell)

Sum of:

* Overhead hours from tickets belonging to the current cell.
* Hours logged against non-billable tickets belonging to the current cell (i.e., tickets from the current cell that are linked to a non-billable account).

#### Ratio of non-billable hours (cell)

Billable hours / total hours logged against tickets belonging to the current cell (e.g. BB, FAL, SE).

#### Remaining non-billable hours

... for the period under review:

`((billable hours * sustainability target) / (1 - sustainability target)) - non-billable hours`

### User-specific sustainability

At this level we can view the sustainability status for a specific team member.

The sustainability dashboard lists:

#### Total hours (user)

Billable hours + non-billable hours logged by the current user.

#### Billable hours (user)

Billable hours logged by the current user.

Hours are considered billable if they are:

1. Logged against billable account.
1. Covered by existing budgets (otherwise they count as overhead and will be added to the total number of non-billable hours).

#### Non-billable hours (user)

Sum of:

* Overhead hours logged by the current user.
* Hours logged against non-billable tickets by the current user (i.e., tickets that are linked to a non-billable account).

#### Ratio of non-billable hours (user)

Billable hours / total hours logged by the current user.

#### Remaining non-billable hours (user)

... for the period under review:

`((billable hours * sustainability target) / (1 - sustainability target)) - non-billable hours`

## Budget Dashboard

This presents a list of all active accounts and the time spent on them from the beginning of the current
year and the goal, based on the budget stored in the DB (see the [Setting up budgets](#setting-up-budgets)
section for setup instructions). For each budget we are listing:

### Account name

With the prefix stripped for better readability.

### Time spent from the beginning of the first year within the selected period

For `Overall` view the cell has green background when budget is on track and turns red when
it's exceeded. This behavior is disabled on cell's and user's dashboards to reduce confusion.

### Goal from the beginning of the first year within the selected period to the end of the next sprint

This field remains the same for all views, because budgets cannot be divided between cells.

### Time spent during the selected period

This field has a checkbox to decide whether to add the value in the sustainability calculations or not

### Goal for the selected period

This field remains the same for all views, because budgets cannot be divided between cells.

### Time scheduled for the incomplete tickets in the current sprint

Time scheduled for the tickets in the next sprint.

### Time that can still be assigned for the next sprint

This field remains the same for all views, because any cell can use the remaining budget.
The cell's background is green when remaining time is greater or equal 0, turns red when it's lower.

### One of the following categories

1. Billable.
2. Non-billable cell.
3. Non-billable non-cell.

### Setting up budgets

To set up the budgets for the accounts you need to:

1. Log into the backend admin (e.g. <http://localhost:8000/admin>) with your superuser account.
2. Go to `Sustainability/Budgets`.
3. Add a new budget for the account.

The budgets are rolling, so these entries are perceived as *changes* of the budgets. It means that
the budget for the account with the specified `name` will be `hours` (per month) up to the next change
or current date.

> E.g. we have the account "Account - Security". From the beginning of 2019 we want the budget to be
> 100h/month, but from September to November (both inclusive) we want to raise it to 200h/month. From
> December and for the whole 2020 it should be lowered back to 100h/month. Therefore, we need
> to create 3 entries via the Django admin:
>
> ```javascript
> [{
>     "name": "Account - Security",
>     "date": "January 2019",
>     "hours": 100
> }, {
>     "name": "Account - Security",
>     "date": "September 2019",
>     "hours": 200
> }, {
>     "name": "Account - Security",
>     "date": "December 2019",
>     "hours": 100
> }]
> ```
>
> Side note: the `date` is a `DateField`, but the example is using simplified representation for brevity.

### Setting up alerts

The alerts are defined in settings to be triggered with Celerybeat. It's possible to subscribe to
specific cell or account alerts via Django admin.

It's also possible to specify addresses that will receive alerts for all existing cells and accounts.
To do this, add email address to `NOTIFICATIONS_SUSTAINABILITY_EMAILS` environment variable.

### Setting up Sustainability Targets

The [sustainability targets](#sustainability-targets) for the individual cells and the company can be
set to different values for sustainability calculations in the [Sustainability Dashboard](#sustainability-dashboard).

1. Go to 'Cells' under 'Sustainability' in the Django admin panel (<http://localhost:8000/admin/sustainability/cell/>).
2. Click 'Add Cell' and enter the Cell's name and it's sustainability target as a decimal value (eg., 0.3 for 30%)

**Note:** For setting the company's sustainability target, create a "Cell" with the company name.

### Setting up webhooks

The SprintCraft app supports triggering webhooks on certain events. Currently, the following events
are supported:

1. 'new sprint' - Triggered at the end of the sprint completion process. It fires a webhook containing
   details of each member of the cell & their responsibilities in the new sprint. It reads permanent
   roles (Sprint Planning Manager etc.) from the `ORGANIZATION_DEFINITION_URL`, and temporary roles
   (Firefighter, Discovery Duty etc.) from the rotations spreadsheets. If the `FEATURE_CELL_ROLES`
   (disabled by default) environment variable is set to `True` it will cause an error and prevent
   the sprint from being completed if the permanent roles cannot be read from the handbook.

In order to set up receivers you first need to set up webhook events; to do that follow these steps:

1. Go to 'Webhook events' in your Django admin panel (<http://localhost:8000/admin/webhooks/webhookevent/>).
2. Click 'Add webhook event' and create events based on the above-mentioned list of events.

For now, only the 'new sprint' event type is supported. More event types will be added in the future.

To create a new webhook receiver, follow these steps:

1. Make sure a 'Webhook Event' exists for your webhook (see the following section for the instructions).
2. Go to 'Webhooks' in the Django admin panel (<http://localhost:8000/admin/webhooks/webhook/>).
3. Click 'Add Webhook'.
4. In Events, select one or multiple events to link to the webhook & enter a payload URL. If you'd
   like to send any extra headers with the request, you can specify them in the header's field using the
   JSON format.

#### For sustainability

Alerts are sent when the ratio of non-billable cell hours to billable hours exceeds
the [sustainability targets](#sustainability-targets) defined for each cell or the
`SUSTAINABILITY_DEFAULT_TARGET` for cells.

By default, these alerts are not being sent. To enable them:

1. Log into the backend admin (e.g. <http://localhost:8000/admin>) with your superuser account.
2. Go to `Sustainability/Cells`.
3. Add new cell.
4. Optionally add comma-separated email addresses that will receive alerts.

#### For budgets

Alerts are sent when time spent from the beginning of the first year within the selected period is
greater than the goal from the beginning of the current year to the end of the next sprint.

Alerts are sent by default to emails specified in the `Suntainability/Cells`.
To subscribe only to specific accounts:

1. Log into the backend admin (e.g. <http://localhost:8000/admin>) with your superuser account.
2. Go to `Sustainability/Accounts`.
3. Add new account.
4. Specify comma-separated email addresses that will receive alerts.

## Automation: Budgets for Billable Clients

This task automatically creates budgets for billable accounts by setting the budget equal to the
number of hours logged in that account during a month. For each billable account that is not marked
as fixed-price, it fetches the hours logged over the course of the previous month and sets them
as the budget for that month.

Note that:

Hours logged *usually* match the number of hours that will be invoiced to the client.
If that is not the case for a given month and account, the budget created by this task
will need to be adjusted manually to make sure that it matches the number of hours invoiced.

Users with a superuser account can trigger the task by sending a HTTP POST request to
`/sustainability/populate_budgets/` either from the Swagger UI (at `/swagger/`) or from other
HTTP clients like cURL, by setting the `Authorization` header to `Token <auth token>`.

**Note**: The auth token can be generated from the backend admin UI.
