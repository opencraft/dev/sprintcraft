[tool.pytest.ini_options]
minversion = '6.0'
addopts = '--ds=config.settings.test --reuse-db'
python_files = ['tests.py', 'test_*.py']

[tool.coverage.run]
include = ['sprintcraft/**']
omit = ['*/migrations/*', '*/tests/*']
plugins = ['django_coverage_plugin']

[tool.ruff]
line-length = 120
exclude = ['migrations', 'docs', 'manage.py']
select = [
    'F',    # Pyflakes
    'E',    # Pycodestyle (errors)
    'W',    # Pycodestyle (warnings)
    'C90',  # mccabe
    'I',    # isort
    'N',    # pep8-naming
#    'D',    # pydocstyle  # TODO: Enable this. Currently, it's applied only to changed lines with `darker`.
    'UP',   # pyupgrade
    'YTT',  # flake8-2020
#    'ANN',  # flake8-annotations  # TODO: Enable this. Currently, it's applied only to changed lines with `darker`.
    'ASYNC',# flake8-async
    'S',    # flake8-bandit
    'BLE',  # flake8-blind-except
    'B',    # flake8-bugbear
    'A',    # flake8-builtins
    'COM',  # flake8-commas
    'C4',   # flake8-comprehensions
#    'DTZ',  # flake8-datetimez  # TODO: Enable this. Currently, it's applied only to changed lines with `darker`.
    'T10',  # flake8-debugger
    'DJ',   # flake8-django
    'EXE',  # flake8-executable
    'FA',   # flake8-future-annotations
    'ISC',  # flake8-implicit-str-concat
    'ICN',  # flake8-import-conventions
    'G',    # flake8-logging-format
    'INP',  # flake8-no-pep420
    'PIE',  # flake8-pie
    'T20',  # flake8-print
    'PYI',  # flake8-pyi
    'PT',   # flake8-pytest-style
    'Q',    # flake8-quotes
    'RSE',  # flake8-raise
    'RET',  # flake8-return
    'SLF',  # flake8-self
    'SIM',  # flake8-simplify
    'TID',  # flake8-tidy-imports
#    'TCH',  # flake8-type-checking  # TODO: Use this.
    'INT',  # flake8-gettext
#    'ARG',  # flake8-unused-arguments  # TODO: Enable this. Currently, it's applied only to changed lines with `darker`.
    'PTH',  # flake8-use-pathlib
    'TD',   # flake8-todos
    'ERA',  # eradicate
    'PGH',  # pygrep-hooks
    'PL',   # Pylint
    'TRY',  # tryceratops
    'FLY',  # flynt
    'RUF',  # Ruff-specific rules
]
ignore = [
    'ANN002', # missing-type-args
    'ANN003', # missing-type-kwargs
    'ANN101', # missing-type-self
    'ANN102', # missing-type-cls
    'ANN204', # missing-return-type-special-method
    'ANN401', # any-type
    'D104',   # undocumented-public-package
    'D105',   # undocumented-magic-method
    'D107',   # undocumented-public-init
    'D200',   # fits-on-one-line
    'D203',   # one-blank-line-before-class
    'D212',   # multi-line-summary-first-line (incompatible with D213)
    'D407',   # dashed-underline-after-section
    'D415',   # ends-in-punctuation (handled by D400)
    'PGH003', # blanket-type-ignore
    'Q000',   # bad-quotes-inline-string
    'TD001',  # invalid-t\odo-tag
    'TD002',  # missing-t\odo-author
    'TD003',  # missing-t\odo-link
    'TRY003', # raise-vanilla-args
]
target-version = 'py311'

[tool.ruff.per-file-ignores]
# assert, missing-return-type-static-method, private-member-access
'*/tests/*' = ['S101', 'ANN205', 'SLF001']
# commented-out-code
'config/settings/*' = ['ERA001']
'config/wsgi.py' = ['ERA001']

[tool.ruff.flake8-annotations]
suppress-none-returning = true

[tool.ruff.flake8-builtins]
builtins-ignorelist = ['list']

# Consider instead of Q000.
#[tool.ruff.flake8-quotes]
#inline-quotes = 'single'

#[tool.ruff.pydocstyle]
#convention = "google"

[tool.ruff.pylint]
allow-magic-value-types = ['int', 'str']

[tool.black]
line-length = 120
target-version = ['py311']
skip_string_normalization = true

[tool.isort]
profile = 'black'
line_length = 120
known_first_party = ['sprintcraft', 'config']
multi_line_output = 3
default_section = 'THIRDPARTY'
skip = ['venv/']
skip_glob = ['**/migrations/*.py']
include_trailing_comma = true
force_grid_wrap = 0
use_parentheses = true

[tool.mypy]
python_version = '3.11'
check_untyped_defs = true
ignore_missing_imports = true
warn_unused_ignores = true
warn_redundant_casts = true
warn_unused_configs = true
plugins = ['mypy_django_plugin.main', 'mypy_drf_plugin.main']

[[tool.mypy.overrides]]
module = ['*.migrations.*', '*.tests.*']
ignore_errors = true

[tool.django-stubs]
django_settings_module = 'config.settings.test'

[tool.darker]
src = ["."]
revision = "origin/master"
diff = true
check = true
isort = false
lint = ['ruff --extend-select ANN,ARG,D,DTZ --ignore ANN002,ANN003,ANN101,ANN102,ANN204,ANN401,D104,D105,D107,D200,D203,D212,D407,D415,TD002,TD003']
log_level = "INFO"
