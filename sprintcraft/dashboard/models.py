"""These are standard Python classes, not Django models. We don't store dashboard in the DB."""
import functools
import re
import typing
from datetime import datetime, timedelta

from dateutil.parser import parse
from django.conf import settings
from jira import Issue
from jira import User as JiraUser
from jira.resources import Sprint

from config.settings.base import SECONDS_IN_HOUR, SECONDS_IN_MINUTE
from sprintcraft.dashboard.libs.google import get_vacations
from sprintcraft.dashboard.libs.jira import CustomJira, QuickFilter
from sprintcraft.dashboard.utils import (
    Cell,
    daterange,
    extract_sprint_id_from_str,
    get_all_sprints,
    get_cell,
    get_cell_members,
    get_issue_fields,
    get_next_sprint,
    get_sprint_end_date,
    get_sprint_meeting_day_division,
    get_sprint_start_date,
    prepare_jql_query,
)


class DashboardIssue:
    """Parses Jira Issue for easier access."""

    def __init__(  # noqa: PLR0913
        self,
        issue: Issue,
        current_sprint_ids: set[int],
        cell_members: list[str],
        unassigned_user: JiraUser,
        other_cell: JiraUser,
        issue_fields: dict[str, str],
        cell_key: str,
    ) -> None:
        self.key = issue.key
        # It should be present, but that's not enforced by the library, so it's better to specify default value.
        self.assignee: JiraUser = getattr(issue.fields, issue_fields['Assignee'], None)
        assignee_not_in_cell = self.assignee and self.assignee.name not in cell_members

        # # This happens when the Jira issue is owned by another cell, but the assignee is a member of the current one
        # (i.e. during a cross-cell assignment).
        if not self.key.startswith(cell_key) and assignee_not_in_cell:
            self.assignee = other_cell

        if not self.assignee:
            self.assignee = unassigned_user

        self.summary = getattr(issue.fields, issue_fields['Summary'], '')
        self.description = getattr(issue.fields, issue_fields['Description'], '')
        self.status = getattr(issue.fields, issue_fields['Status']).name
        self.time_spent = getattr(issue.fields, issue_fields['Time Spent'], 0) or 0
        self.time_estimate = getattr(issue.fields, issue_fields['Remaining Estimate'], 0) or 0
        self.is_epic = getattr(issue.fields, issue_fields['Issue Type']).name == 'Epic'
        try:
            self.account = getattr(issue.fields, issue_fields['Account']).name
        except AttributeError:  # Inherited account in a subtask is null
            self.account = None

        try:
            sprint = getattr(issue.fields, issue_fields['Sprint'])
            if isinstance(sprint, list):
                sprint = sprint[-1]
            self.current_sprint = extract_sprint_id_from_str(sprint) in current_sprint_ids
        except (AttributeError, TypeError):
            # Possible for epics
            self.current_sprint = False
        self.story_points = getattr(issue.fields, issue_fields['Story Points'], None)
        self.reviewer_1: JiraUser = getattr(issue.fields, issue_fields['Reviewer 1'], "Unassigned")
        if not self.reviewer_1:
            self.reviewer_1 = unassigned_user
        # We don't want to treat commitments from the other cell as "Unassigned".
        elif self.reviewer_1.name not in cell_members:
            self.reviewer_1 = other_cell

        self.is_relevant = self._is_relevant_for_current_cell(cell_key, cell_members)
        self.is_flagged = self._is_flagged(issue, issue_fields)

    def get_bot_directive(self, pattern) -> int:
        """
        Retrieves time (in seconds) specified with special directives placed for the Jira bot in ticket's description.
        :returns `int` with duration (converted to seconds) defined with the directive.
        :raises `ValueError` if directive was not found.
        """
        try:
            search = re.search(pattern, self.description).groupdict('0')  # type: ignore
            hours = int(search.get('hours', 0))
            minutes = int(search.get('minutes', 0))
        except (AttributeError, TypeError) as e:  # Directive not found or description is `None`.
            raise ValueError('The directive was not found.') from e
        else:
            return hours * SECONDS_IN_HOUR + minutes * SECONDS_IN_MINUTE

    @functools.cached_property
    def assignee_time(self) -> int:
        """Calculate time needed by the assignee of the issue."""
        if self.is_epic:
            return 0

        # Assume that no more review will be needed at this point.
        if self.status in (settings.SPRINT_STATUS_EXTERNAL_REVIEW, settings.SPRINT_STATUS_MERGED):
            return self.time_estimate

        return max(self.time_estimate - self.review_time, 0)  # We don't want negative values here.

    def calculate_review_time_from_story_points(self) -> float:
        """
        Calculate time needed for a review, by using the story points defined in the issue.

        If the story points set in the issue are defined in `SPRINT_HOURS_RESERVED_FOR_REVIEW`, then
        it is going to return the review time for that value.
        Otherwise, it will use the review time of the closest value defined in `SPRINT_HOURS_RESERVED_FOR_REVIEW`.
        """
        issue_story_points = self.story_points
        review_hours_by_story_points = settings.SPRINT_HOURS_RESERVED_FOR_REVIEW
        # If the story points value is defined in SPRINT_HOURS_RESERVED_FOR_REVIEW setting, then return it
        if (hours := review_hours_by_story_points.get(issue_story_points)) is not None:
            return hours

        # As the story points value is not defined in SPRINT_HOURS_RESERVED_FOR_REVIEW setting, then we are going
        # to use the review time for the closest story points value defined
        defined_story_points = sorted(key for key in review_hours_by_story_points if key is not None)
        if not defined_story_points:
            # If not numerical values were defined, then we will use the "null"/undefined value
            return review_hours_by_story_points[None]
        nearest_story_points = min(
            defined_story_points,
            key=lambda story_points: abs(story_points - issue_story_points),
        )
        return review_hours_by_story_points[nearest_story_points]

    @functools.cached_property
    def review_time(self) -> int:
        """
        Get time needed for the review.
        Unless directly specified (with Jira bot directive), we're planning by using the
        SPRINT_HOURS_RESERVED_FOR_REVIEW setting.
        """
        try:
            return self.get_bot_directive(settings.SPRINT_REVIEW_DIRECTIVE)

        except ValueError:
            # If we want to plan review time for epic or ticket with `SPRINT_STATUS_NO_MORE_REVIEW` status,
            # we need to specify it with bot's directive.
            if self.is_epic or self.status in settings.SPRINT_STATUS_NO_MORE_REVIEW:
                return 0

            return int(SECONDS_IN_HOUR * self.calculate_review_time_from_story_points())

    @functools.cached_property
    def recurring_time(self) -> int:
        """Get required assignee time for the recurring story."""
        if self.status == settings.SPRINT_STATUS_RECURRING:
            try:
                return self.get_bot_directive(settings.SPRINT_RECURRING_DIRECTIVE)
            except ValueError:  # Directive not found.
                pass
        return 0

    @functools.cached_property
    def epic_management_time(self) -> int:
        """Get required assignee time for managing the epic."""
        if not self.is_epic:
            return 0

        try:
            return self.get_bot_directive(settings.SPRINT_EPIC_DIRECTIVE)
        except ValueError:
            return settings.SPRINT_HOURS_RESERVED_FOR_EPIC_MANAGEMENT * SECONDS_IN_HOUR

    def _is_relevant_for_current_cell(self, cell_key: str, cell_members: list[str]) -> bool:
        """
        Helper method for determining whether the issue is "relevant" for the current cell.
        The issue is "relevant" when it matches one of the following conditions:
        - The jira user has a cross cell assignment
        - The jira issue belongs to the given cell

        Note: currently we don't consider reviewer 2 role in SprintCraft.
        """

        cross_cell_assignment = self.assignee.name in cell_members or self.reviewer_1.name in cell_members
        return self.key.startswith(cell_key) or cross_cell_assignment

    @staticmethod
    def _is_flagged(issue: Issue, issue_fields: dict[str, str]) -> bool:
        """Check whether the ticket has been flagged as "Impediment"."""
        return any(filter(lambda x: x.value == 'Impediment', getattr(issue.fields, issue_fields['Flagged']) or []))


class DashboardRow:
    """Represents single dashboard row (user)."""

    def __init__(self, user: JiraUser) -> None:
        self.user = user
        self.current_remaining_assignee_time = 0
        self.current_remaining_review_time = 0
        self.current_remaining_upstream_time = 0
        self.current_vacation_time = 0
        self.current_sprint_length = 0
        self.future_assignee_time = 0
        self.future_review_time = 0
        self.future_epic_management_time = 0
        self.goal_time = 0
        self.flagged_time = 0
        self.current_unestimated: list[DashboardIssue] = []
        self.future_unestimated: list[DashboardIssue] = []
        self.future_vacation_time = 0
        self.future_sprint_length = 0

    @property
    def committed_time(self) -> float:
        """Calculate summary time for the upcoming sprint."""
        return (
            self.current_remaining_assignee_time
            + self.current_remaining_review_time
            + self.current_remaining_upstream_time
            + self.future_assignee_time
            + self.future_review_time
            + self.future_epic_management_time
        )

    @property
    def remaining_time(self) -> float:
        """Calculate available time for the upcoming sprint."""
        return self.goal_time - self.committed_time

    def add_unestimated_issue(self, issue: DashboardIssue) -> None:
        """Add non-estimated issue to the list of unestimated issues."""
        if issue.current_sprint:
            self.current_unestimated.append(issue)
        else:
            self.future_unestimated.append(issue)


class Dashboard:
    """Aggregates user records into a dashboard."""

    def __init__(self, board_id: int, conn: CustomJira) -> None:
        self.jira_connection = conn
        self.dashboard: dict[JiraUser, DashboardRow] = {}
        self.issue_fields: dict[str, str]
        self.issues: list[DashboardIssue]
        self.members: list[str]
        self.commitments: dict[str, dict[str, dict[str, int]]] = {}
        self.board_id = board_id
        self.cell: Cell
        self.active_sprints: list[Sprint]
        self.cell_future_sprint: Sprint
        self.future_sprints: list[Sprint]
        self.future_sprint_start: str
        self.future_sprint_end: str

        # Retrieve data from Jira.
        self.cell = get_cell(conn, board_id)
        self.today = datetime.now().strftime(settings.JIRA_API_DATE_FORMAT)
        self.get_sprints()
        self.create_mock_users()
        self.vacations = get_vacations(min(self.today, self.before_future_sprint_start), self.after_future_sprint_end)
        self.get_issues()
        self.generate_rows()

    @property
    def rows(self):
        """Simplification for the serializer."""
        return self.dashboard.values()

    def get_sprints(self) -> None:
        """Retrieves current and future sprint for the board."""
        sprints = get_all_sprints(self.jira_connection, self.board_id)
        self.active_sprints = sprints['active']
        self.future_sprints = sprints['future']
        self.cell_future_sprint = get_next_sprint(sprints['cell'], sprints['cell'][0])

        self.future_sprint_start = get_sprint_start_date(self.cell_future_sprint)
        self.future_sprint_end = get_sprint_end_date(self.cell_future_sprint)

        # Helper variables to retrieve more data for different timezones (one extra day on each end of the sprint).
        self.before_future_sprint_start = (parse(self.future_sprint_start) - timedelta(days=1)).strftime(
            settings.JIRA_API_DATE_FORMAT,
        )
        self.after_future_sprint_end = (parse(self.future_sprint_end) + timedelta(days=1)).strftime(
            settings.JIRA_API_DATE_FORMAT,
        )

    def create_mock_users(self):
        """Create mock users for handling unassigned and cross-cell tickets."""
        self.unassigned_user = JiraUser(self.jira_connection._options, self.jira_connection._session)  # noqa: SLF001
        self.unassigned_user.name = "Unassigned"
        self.unassigned_user.displayName = self.unassigned_user.name

        # We don't want to treat commitments from the other cell as "Unassigned".
        self.other_cell = JiraUser(self.jira_connection._options, self.jira_connection._session)  # noqa: SLF001
        self.other_cell.name = "Other Cell"
        self.other_cell.displayName = "Other Cell"

    def delete_mock_users(self) -> None:
        """Remove mock users from the dashboard. It's useful for exporting commitments."""
        self.dashboard.pop(self.other_cell, None)
        self.dashboard.pop(self.unassigned_user, None)

    def get_issues(self) -> None:
        """Retrieves all stories and epics for the current dashboard."""
        self.issue_fields = get_issue_fields(self.jira_connection, settings.JIRA_REQUIRED_FIELDS)

        issues: list[Issue] = self.jira_connection.search_issues(
            **prepare_jql_query(
                [str(sprint.id) for sprint in self.active_sprints + self.future_sprints],
                list(self.issue_fields.values()),
            ),
            maxResults=0,
        )
        quickfilters: list[QuickFilter] = self.jira_connection.quickfilters(self.board_id)

        self.members = get_cell_members(quickfilters)
        self.sprint_division = get_sprint_meeting_day_division(self.future_sprint_start)
        self.issues = []

        active_sprint_ids = {sprint.id for sprint in self.active_sprints}
        for issue in issues:
            dashboard_issue = DashboardIssue(
                issue,
                active_sprint_ids,
                self.members,
                self.unassigned_user,
                self.other_cell,
                self.issue_fields,
                self.cell.key,
            )
            if dashboard_issue.is_relevant:
                self.issues.append(dashboard_issue)

        for member in self.members:
            schedule = self.jira_connection.user_schedule(
                member,
                min(self.today, self.before_future_sprint_start),
                self.after_future_sprint_end,
            )
            sprint_start_index = (parse(self.before_future_sprint_start) - parse(self.today)).days
            self.commitments[member] = {
                'total_current_sprint': sum([day.requiredSeconds for day in schedule.days[0 : sprint_start_index + 1]]),
                'total_future_sprint': sum([day.requiredSeconds for day in schedule.days[sprint_start_index:]]),
                'days': {day.date: day.requiredSeconds for day in schedule.days},
            }

    @typing.no_type_check
    def generate_rows(self) -> None:
        """Generates rows for all users and calculates their time stats."""
        for issue in self.issues:  # type: DashboardIssue
            assignee = self.dashboard.setdefault(issue.assignee, DashboardRow(issue.assignee))
            reviewer_1 = self.dashboard.setdefault(issue.reviewer_1, DashboardRow(issue.reviewer_1))

            # Calculate time for epic management
            if issue.is_epic:
                assignee.future_epic_management_time += issue.epic_management_time
                continue

            # Calculate hours for recurring tickets for the upcoming sprint.
            if issue.status == settings.SPRINT_STATUS_RECURRING:
                assignee.future_assignee_time += issue.recurring_time
                reviewer_1.future_review_time += issue.review_time
                continue

            # Check if the issue has any time left.
            if issue.time_estimate == 0:
                assignee.add_unestimated_issue(issue)

            # Calculations for the current sprint.
            if issue.current_sprint:
                if issue.status == settings.SPRINT_STATUS_EXTERNAL_REVIEW:
                    reviewer_1.current_remaining_upstream_time += issue.review_time
                    assignee.current_remaining_upstream_time += issue.assignee_time

                else:
                    reviewer_1.current_remaining_review_time += issue.review_time
                    assignee.current_remaining_assignee_time += issue.assignee_time

            # Calculations for the upcoming sprint.
            else:
                assignee.future_assignee_time += issue.assignee_time
                reviewer_1.future_review_time += issue.review_time

                if issue.is_flagged:
                    assignee.flagged_time += issue.assignee_time
                    reviewer_1.flagged_time += issue.review_time

        self.dashboard.pop(self.other_cell, None)

        # Hide users that are not included in the Sprint board's quickfilters.
        for user in list(self.dashboard.keys()):
            if user != self.unassigned_user and user.name not in self.members:
                self.dashboard.pop(user)

        self._calculate_commitments()

    @typing.no_type_check
    def _calculate_commitments(self):
        """
        Calculates time commitments and vacations for each user.
        """
        for row in self.rows:
            if row.user != self.unassigned_user:
                # Calculate vacations
                for vacation in self.vacations:
                    if row.user.displayName.startswith(vacation["user"]):
                        # Vacations in the current sprint.
                        for vacation_date in daterange(
                            max(
                                vacation["start"]["date"],
                                self.today,
                            ),
                            min(
                                vacation["end"]["date"],
                                self.before_future_sprint_start,
                            ),
                        ):
                            row.current_vacation_time += self._get_vacation_for_day(
                                self.commitments[row.user.name]["days"][vacation_date],
                                vacation_date,
                                vacation["seconds"],
                                row.user.displayName,
                                False,
                            )

                        # Vacations in the future sprint.
                        for vacation_date in daterange(
                            max(
                                vacation["start"]["date"],
                                self.before_future_sprint_start,
                            ),
                            min(
                                vacation["end"]["date"],
                                self.after_future_sprint_end,
                            ),
                        ):
                            row.future_vacation_time += self._get_vacation_for_day(
                                self.commitments[row.user.name]["days"][vacation_date],
                                vacation_date,
                                vacation["seconds"],
                                row.user.displayName,
                                True,
                            )
                    elif row.user.displayName < vacation["user"]:
                        # Small optimization, as users' vacations are sorted.
                        break

                row.current_sprint_length = self.commitments[row.user.name]["total_current_sprint"]
                # Remove the "padding" from a day before and after the sprint, and reserved time for planning.
                row.future_sprint_length = (
                    self.commitments[row.user.name]["total_future_sprint"]
                    - self.commitments[row.user.name]["days"][self.before_future_sprint_start]
                    - self.commitments[row.user.name]["days"][self.after_future_sprint_end]
                    - settings.SPRINT_HOURS_RESERVED_FOR_PLANNING * SECONDS_IN_HOUR
                )
                row.goal_time = row.future_sprint_length - row.future_vacation_time

    def _get_vacation_for_day(  # noqa: PLR0913
        self,
        commitments: int,
        date: str,
        planned_commitments: int,
        username: str,
        for_next_sprint: bool,
    ) -> float:
        """
        Returns vacation time for specific users during a day.

        Because of the timezones we need to consider 4 edge cases. When vacations are scheduled:
        1. For the last day of the active sprint, there are two subcases:
            a. Positive timezone - this day is completely a part of the active sprint, so this time is ignored (0).
            b. Negative timezone - this day can span the active and next sprint, because user can work after the sprint
               ends. The ratio is represented by `1 - division`.
               If it does not span, then it is treated as a part of the active sprint.
        2. For the first day of the next sprint, there are two subcases:
            a. Positive timezone - this day can span the active and next sprint, because user can work after the sprint
               ends. The ratio is represented by `1 - division`.
               If it does not span, then it is treated as a part of the next sprint.
            b. Negative timezone - this day is completely a part of the next sprint, so it is counted as vacations.
        3. For the last day of the next sprint, there are two subcases:
            a. Positive timezone - this day is completely a part of the next sprint, so it is counted as vacations.
            b. Negative timezone - this day can span the next and future next sprint, because user can work after
               the sprint ends. The ratio is represented by `division`.
               If it does not span, then it is treated as a part of the next sprint.
        4. For the first day of the future next sprint, there are two subcases:
            a. Positive timezone - this day can span the next and future next sprint, because user can work after
               the sprint ends. The ratio is represented by `division`.
               If it does not span, then it is treated as a part of the future next sprint.
            b. Negative timezone - this day is completely a part of the future next sprint, so this time is ignored (0).

        `division` - a part of the user's availability before the start of the sprint.

        TODO: Check whether this works correctly with other sprint start times than midnight UTC.
              For these it can span 3 days, so we might have 6 (or even more) corner cases.

        :param for_next_sprint: Whether the context of the vacation is for the next, or the current sprint.
        """
        division, positive_timezone = self.sprint_division[username]
        vacations = commitments - planned_commitments
        # For negative timezones non-overlapping days are treated the same way as they are for the positive ones.
        # I.e. if users are working from 9am to 5pm UTC, it will be the same day regardless of whether they are working
        # within UTC-1 or UTC+1.
        if not positive_timezone:
            division = division or 1

        if for_next_sprint:
            sprint_begin = self.future_sprint_start
            sprint_end = self.future_sprint_end
        else:
            # TODO: Add tests for this scenario.
            sprint_begin = self.today
            sprint_end = self.future_sprint_start

        if date < sprint_begin:
            return vacations * (1 - division) if not positive_timezone else 0
        if date == sprint_begin:
            return vacations * (1 - division) if positive_timezone else vacations
        if date == sprint_end:
            return vacations * division if not positive_timezone else vacations
        if date > sprint_end:
            return vacations * division if positive_timezone else 0

        return vacations
