"""
Tests for celery tasks.
"""

from collections import defaultdict
from unittest.mock import Mock, call, patch

import pytest

from sprintcraft.dashboard.libs.tests.fixtures import rotations_spreadsheet
from sprintcraft.dashboard.tasks import trigger_new_sprint_webhooks_task
from sprintcraft.dashboard.tests.fixtures import ROLES_DICT
from sprintcraft.dashboard.tests.test_helpers import MockItem
from sprintcraft.dashboard.tests.test_utils import MockUser
from sprintcraft.webhooks.tests.factories import WebhookEventFactory, WebhookFactory

pytestmark = pytest.mark.django_db


@patch('sprintcraft.dashboard.libs.google.get_rotations_spreadsheet', return_value=rotations_spreadsheet)
@patch('sprintcraft.dashboard.tasks.get_cell_member_roles', return_value=ROLES_DICT)
@patch('sprintcraft.dashboard.tasks.connect_to_jira')
@patch('sprintcraft.dashboard.tasks.trigger_webhook')
def test_trigger_webhooks(
    mock_trigger_webhook: Mock,
    mock_connect_to_jira: Mock,
    _mock_get_roles: Mock,  # noqa: PT019
    _mock_spreadsheet: Mock,  # noqa: PT019
):
    """
    Test that webhooks are triggered by the sprint completion task.
    """
    mock_conn = Mock()
    # connect_to_jira() produces a context manager, so we have a few layers of
    # indirection.
    mock_connect_to_jira.return_value.__enter__.return_value = mock_conn
    mock_conn.quickfilters.return_value = [
        MockItem(query=f'assignee = {name} or reviewer_1 = {name} or reviewer_2 = {name}')
        for name in ['jane', 'jack', 'john', 'jake']
    ]
    mock_conn.user.side_effect = MockUser.list()
    event = WebhookEventFactory(name='new sprint')
    webhook1 = WebhookFactory()
    webhook2 = WebhookFactory()
    webhook1.events.add(event)
    webhook2.events.add(event)
    # The trigger should only be called twice, and not with this one.
    WebhookFactory()
    trigger_new_sprint_webhooks_task(
        'Bebop',
        'BB.336 (2024-10-15)',
        336,
        26,
        '2024-10-15T00:00:00.000Z',
        '2024-10-28T00:00:00.000Z',
    )
    participants = defaultdict(list)
    participants.update(
        {
            'jackdoe@opencraft.com': [],
            'jakedoe@opencraft.com': [],
            'janedoe@opencraft.com': [],
            'johndoe@opencraft.com': [],
        },
    )
    payload = {
        'board_id': 26,
        'cell': 'Bebop',
        'sprint_number': 336,
        'sprint_name': 'BB.336 (2024-10-15)',
        'participants': participants,
        'event_name': 'new sprint',
        'start_date': '2024-10-15T00:00:00+00:00',
        'end_date': '2024-10-29T00:00:00+00:00',
    }
    mock_trigger_webhook.delay.assert_has_calls([call(webhook1.id, payload), call(webhook2.id, payload)])
    assert mock_trigger_webhook.delay.call_count == 2
