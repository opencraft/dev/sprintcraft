import calendar
import logging
from datetime import datetime, timedelta
from string import Template

from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.cache import cache
from django.core.mail import send_mail

from config import celery_app

from .models import Account, Budget, Cell, SustainabilityDashboard
from .utils import generate_month_range

logger = logging.getLogger(__name__)


@celery_app.task()
def validate_worklog_cache(long_term=True, force_regenerate=False) -> bool:
    """
    Validate worklog caches for all months in the specified range and regenerates them, if required.
    It uses `settings.CACHE_WORKLOG_REGENERATE_LOCK` lock to avoid race conditions during regenerating long term caches.

    :param long_term: if true then checks all worklogs from `settings.TEMPO_START_YEAR` until the end of current month.
           Otherwise checks only the last `settings.CACHE_WORKLOG_MUTABLE_MONTHS` mutable months.
    :param force_regenerate: If true, regenerates cache for each month in the specified range.
    """
    today = datetime.today()

    if not cache.add(
        settings.CACHE_WORKLOG_REGENERATE_LOCK,
        True,
        settings.CACHE_WORKLOG_REGENERATE_LOCK_TIMEOUT_SECONDS,
    ):  # Cache regeneration is still running or has ended unsuccessfully.
        return False

    if long_term:
        # Validate cache for all one-month periods from the beginning of `settings.TEMPO_START_YEAR` to the present one.
        start_year = parse(str(settings.TEMPO_START_YEAR))
        start_date = start_year.replace(month=1, day=1)
        cache_timeout = settings.CACHE_WORKLOG_TIMEOUT_LONG_TERM
        end_date = today - relativedelta(months=settings.CACHE_WORKLOG_MUTABLE_MONTHS)
        last_day_of_month = calendar.monthrange(end_date.year, end_date.month)[1]
        end_date = end_date.replace(day=last_day_of_month)
    else:
        # Validate cache only for the last `settings.CACHE_WORKLOG_MUTABLE_MONTHS` mutable months.
        start_date = (today - relativedelta(months=settings.CACHE_WORKLOG_MUTABLE_MONTHS - 1)).replace(day=1)
        cache_timeout = settings.CACHE_WORKLOG_TIMEOUT_SHORT_TERM
        last_day_of_month = calendar.monthrange(today.year, today.month)[1]
        end_date = today.replace(day=last_day_of_month)

    start_str = start_date.strftime(settings.JIRA_API_DATE_FORMAT)
    end_str = end_date.strftime(settings.JIRA_API_DATE_FORMAT)

    for month_start, month_end in generate_month_range(start_str, end_str):
        SustainabilityDashboard.fetch_accounts_chunk(
            str(month_start),
            str(month_end),
            cache_timeout=cache_timeout,
            force=force_regenerate,
        )

    cache.delete(settings.CACHE_WORKLOG_REGENERATE_LOCK)
    return True


@celery_app.task()
def send_email_alerts() -> bool:
    """
    Send email alerts to users that keep track of the sustainability and budgets.

    Email addresses can be specified via:
        - `NOTIFICATIONS_SUSTAINABILITY_EMAILS` env variable - this subscribes an email for ALL notifications,
        - `Cell`/`Account` models of Django admin - only for specific cells/accounts.

    Note: notifications for projects that are not added to `Cells` (e.g. via Django admin) will be skipped for brevity.
    """
    today = datetime.today()
    last_day_of_month = calendar.monthrange(today.year, today.month)[1]
    start_date = today.replace(day=1)
    end_date = today.replace(day=last_day_of_month)
    start_str = start_date.strftime(settings.JIRA_API_DATE_FORMAT)
    end_str = end_date.strftime(settings.JIRA_API_DATE_FORMAT)
    dashboard = SustainabilityDashboard(start_str, end_str)

    # Send alerts for cells.
    unsustainable_cells: dict[str, float] = {}
    for cell, ratio in dashboard.get_projects_sustainability().items():
        try:
            db_cell = Cell.objects.get(name=cell)
            max_percentage_ratio = db_cell.sustainability_target * 100
            if ratio >= max_percentage_ratio:
                unsustainable_cells[cell] = ratio
        except Cell.DoesNotExist:
            # Ignore non-set projects.
            pass

    _send_email_alert(
        unsustainable_cells,
        "Cell sustainability problem.",
        Template(
            f"The sustainability of $entity is currently $ratio%. It should be lower than {max_percentage_ratio}%.",
        ),
        Cell,
    )

    # Send alerts for budgets.
    exceeded_budgets: dict[str, float] = {}
    for account, remaining in dashboard.get_accounts_remaining_time().items():
        if remaining < 0:
            exceeded_budgets[account] = -remaining

    _send_email_alert(
        exceeded_budgets,
        "Budget problem.",
        Template("The $entity account's budget is exceeded by $ratio."),
        Account,
    )

    return True


def _send_email_alert(budgets: dict[str, float], title: str, message: Template, model: type[Cell] | type[Account]):
    """
    Helper method for sending emails for Cells or Accounts.
    """
    for key, value in budgets.items():
        emails = settings.NOTIFICATIONS_SUSTAINABILITY_EMAILS
        try:  # noqa: SIM105
            emails.extend(model.objects.get(name=key).alert_emails)
        except (model.DoesNotExist, AttributeError):  # Emails not defined in the DB.
            pass

        if emails:
            send_mail(
                title,
                message.substitute(entity=key, ratio=str(round(value, 2))),
                settings.DEFAULT_FROM_EMAIL,
                emails,
            )


@celery_app.task()
def populate_budget_for_billable_accounts():
    """
    This task is supposed to run once per month, for the previous month.

    For each billable account that is not marked as fixed-price, it fetches
    the hours logged over the course of the previous month and sets them
    as the budget for that month.
    """
    today = datetime.now()
    end = today.replace(day=1) - timedelta(days=1)
    start = end.replace(day=1)
    start_str = start.strftime(settings.JIRA_API_DATE_FORMAT)
    end_str = end.strftime(settings.JIRA_API_DATE_FORMAT)

    dashboard = SustainabilityDashboard(start_str, end_str)

    for sustainbility_account in dashboard.billable_accounts:
        if isinstance(sustainbility_account, str):
            # To satisfy mypy - it infers this could be a str.
            continue
        # Each sustainability account should have a corresponding account to determine
        # if it is a fixed budget or not. Let's create one if it doesn't exist with the
        # default of it NOT being a fixed budget project.
        account, created = Account.objects.get_or_create(name=sustainbility_account.name)
        if created:
            logger.info("Created a new Account: %s", account)

        if not account.is_fixed_price:
            Budget.objects.update_or_create(
                account=account,
                date=start,
                defaults={"hours": round(sustainbility_account.overall)},
            )
