from unittest.mock import patch

import pytest

from sprintcraft.sustainability.models import Account, Budget, SustainabilityAccount
from sprintcraft.sustainability.tasks import populate_budget_for_billable_accounts


@pytest.mark.django_db()
@patch('sprintcraft.sustainability.tasks.SustainabilityDashboard', autospec=True)
class TestPopulateBudgetTask:
    def test_no_changes_with_no_billable_accounts(self, mock_dashboard):
        mock_dashboard.return_value.billable_accounts = []
        assert Budget.objects.count() == 0

        populate_budget_for_billable_accounts()

        assert Budget.objects.count() == 0

    def test_create_account_and_budget_for_all_billable_accounts(self, mock_dashboard):
        mock_dashboard.return_value.billable_accounts = [
            SustainabilityAccount("Test Account 1"),
            SustainabilityAccount("Test Account 2"),
            SustainabilityAccount("Test Account 3"),
        ]
        assert Budget.objects.count() == 0
        assert Account.objects.count() == 0

        populate_budget_for_billable_accounts()

        mock_dashboard.assert_called()
        assert Account.objects.count() == 3
        assert Budget.objects.count() == 3

    def test_budget_is_not_added_for_fixed_price_projects(self, mock_dashboard):
        mock_dashboard.return_value.billable_accounts = [
            SustainabilityAccount("Regular Account"),
            SustainabilityAccount("Fixed Price"),
        ]
        Account.objects.create(name="Fixed Price", is_fixed_price=True)
        assert Budget.objects.count() == 0

        populate_budget_for_billable_accounts()

        assert Budget.objects.count() == 1
        assert Budget.objects.first().account.name == "Regular Account"
