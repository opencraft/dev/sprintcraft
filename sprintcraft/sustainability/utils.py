import datetime
from collections.abc import Generator, Iterable

from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.cache import cache
from jira import JIRAError, Worklog

from sprintcraft.dashboard.libs.jira import Account, chunks, connect_to_jira


def split_accounts_into_categories(accounts: list[Account]) -> dict[str, list[Account]]:
    """
    Converts `List[Account]` into the following format: `Dict[category: str, List[Account]]`.
    """
    result: dict[str, list[Account]] = {}
    for account in accounts:
        try:
            category = account.category.key
        except AttributeError:
            category = ''

        result.setdefault(category, []).append(account)
    return result


def generate_month_range(start: str, end: str) -> Generator[tuple[datetime.date, datetime.date], None, None]:
    """Generates months between `start` and `end` dates."""
    start_date = parse(start)
    end_date = parse(end)

    current_date = start_date.replace(day=1)  # First day of each month
    while current_date <= end_date:
        last_date_of_month = current_date + relativedelta(day=31)
        yield max(current_date, start_date), min(last_date_of_month, end_date)
        current_date += relativedelta(months=+1)


def diff_month(start: datetime.date, end: datetime.date) -> int:
    """
    Returns how many months are between two dates.
    The result is rounded up, which means that there is always one month between dates within the same month.
    """
    if start > end:
        raise AttributeError("The start cannot be after the end.")
    return (end.year - start.year) * 12 + end.month - start.month + 1


def _get_cached_dicts_from_keys(keys: Iterable) -> dict:
    """Helper function for retrieving aggregated cache results."""
    result: dict = {}
    for key in keys:
        cached_chunk = cache.get(key, {})
        result.update(cached_chunk)

    return result


def cache_worklogs_and_issues(required_worklogs: set[str], long_term: bool) -> dict[str, dict[str, str]]:
    """
    Workaround for missing Tempo API data. It retrieves `required_worklogs` and caches them along with issues.

    It's possible to regenerate long-term cache by specifying `long_term` argument.
    """
    # Determine whether we're be using long-term cache or short-term one. Set keys and timeout accordingly.
    worklogs_key = settings.CACHE_WORKLOGS_KEY_LONG_TERM if long_term else settings.CACHE_WORKLOGS_KEY
    issues_key = settings.CACHE_ISSUES_KEY_LONG_TERM if long_term else settings.CACHE_ISSUES_KEY
    timeout = settings.CACHE_WORKLOG_TIMEOUT_LONG_TERM if long_term else settings.CACHE_ISSUES_TIMEOUT_SHOT_TERM

    # Check if worklogs are missing from cache.
    required_issues: set[str] = set()
    worklogs: dict[str, dict[str, str]] = (
        _get_cached_dicts_from_keys((settings.CACHE_WORKLOGS_KEY, settings.CACHE_WORKLOGS_KEY_LONG_TERM))
        if not long_term
        else {}
    )

    if missing_worklogs := required_worklogs - worklogs.keys():
        with connect_to_jira() as conn:
            retrieved_worklogs: list[Worklog] = conn.worklog_list(list(missing_worklogs))  # type: ignore
            for worklog in retrieved_worklogs:
                required_issues.add(worklog.issueId)

        # Check if worklogs are missing from cache.
        issues: dict[str, dict[str, str]] = (
            _get_cached_dicts_from_keys((settings.CACHE_ISSUES_KEY, settings.CACHE_ISSUES_KEY_LONG_TERM))
            if not long_term
            else {}
        )
        new_issues: dict[str, dict[str, str]] = {}

        if missing_issues := required_issues - issues.keys():
            with connect_to_jira() as conn:
                try:
                    retrieved_issues = conn.search_issues(
                        f'id in ({",".join(missing_issues)})',
                        fields='project',
                        maxResults=0,
                    )
                except JIRAError:
                    # We can notice this for long-term cache, as Jira has limits for the header size.
                    retrieved_issues = []
                    for chunk in chunks(list(missing_issues), 12):
                        retrieved_issues = conn.search_issues(
                            f'id in ({",".join(chunk)})',
                            fields='project',
                            maxResults=0,
                        )
            new_issues = {
                issue.id: {'key': issue.key, 'project': issue.fields.project.name} for issue in retrieved_issues
            }

        new_worklogs = {
            worklog.id: issues.get(worklog.issueId, new_issues.get(worklog.issueId)) for worklog in retrieved_worklogs
        }

        # We retrieve cache second time to avoid race conditions.
        with cache.lock(settings.CACHE_ISSUES_LOCK, timeout=settings.CACHE_ISSUES_LOCK_TIMEOUT_SECONDS):  # type: ignore
            if new_issues:
                issues = cache.get(issues_key) or {}
                issues.update(new_issues)
                cache.set(issues_key, issues, timeout)

            worklogs = cache.get(worklogs_key) or {}
            worklogs.update(new_worklogs)
            cache.set(worklogs_key, worklogs, timeout)

    return worklogs


def on_error(e: BaseException):
    """Callback method for retrieving exceptions from async processes."""
    raise e
