"""
DRF Views for the sustainability app.
"""

from drf_spectacular.utils import OpenApiParameter, OpenApiResponse, extend_schema
from rest_framework import authentication, permissions, views, viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from sprintcraft.sustainability.models import Account, SustainabilityDashboard
from sprintcraft.sustainability.serializers import AccountSerializer, SustainabilityDashboardSerializer
from sprintcraft.sustainability.tasks import populate_budget_for_billable_accounts

_from_param = OpenApiParameter(
    'from',
    location='query',
    description="Start date in format `%Y-%M-%d`.",
    type=str,
)
_to_param = OpenApiParameter(
    'to',
    location='query',
    description="End date in format `%Y-%M-%d`.",
    type=str,
)
_sustainability_response = OpenApiResponse(
    description='Sustainability dashboard for the selected period.',
    response=SustainabilityDashboardSerializer,
)


# noinspection PyMethodMayBeStatic
class SustainabilityDashboardViewSet(viewsets.ViewSet):
    """
    Generates sustainability stats (billable, non-billable, non-billable-cell-responsible hours) within date range.

    You should either use both `from` and `to` query params here.
    """

    permission_classes = (permissions.IsAuthenticated,)

    @extend_schema(parameters=[_from_param, _to_param], responses={200: _sustainability_response})
    def list(self, request):
        from_ = request.query_params.get('from')
        to = request.query_params.get('to')
        if not (from_ and to):
            raise ValidationError("`from` and `to` query params are required.")

        dashboard = SustainabilityDashboard(from_, to)
        serializer = SustainabilityDashboardSerializer(dashboard)
        return Response(serializer.data)


class PopulateBudgetsView(views.APIView):
    """
    Trigger the background task to populate previous month's budgets for all billable accounts.
    """

    authentication_classes = [authentication.SessionAuthentication, authentication.TokenAuthentication]
    permission_classes = (permissions.IsAdminUser,)

    @extend_schema(responses={200: "OK"})
    def post(self, request):
        populate_budget_for_billable_accounts.delay()
        return Response("OK")


class AccountsViewSet(viewsets.ModelViewSet):
    """
    Views to manage SprintCraft accounts.
    """

    queryset = Account.objects.all()
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = AccountSerializer
