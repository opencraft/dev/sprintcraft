from typing import Any

from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.utils import perform_login
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _

from config.settings.base import ACCOUNT_ALLOWED_EMAIL_DOMAINS, FRONTEND_URL


class AccountAdapter(DefaultAccountAdapter):
    def clean_email(self, email):
        """This ensures that the provided domain is specified in the `ACCOUNT_ALLOWED_EMAIL_DOMAINS`."""
        domain = email.split('@')[-1]
        if domain not in ACCOUNT_ALLOWED_EMAIL_DOMAINS:
            raise ValidationError(_("Registration from this email domain is not allowed."))
        return email

    def is_open_for_signup(self, request: HttpRequest):
        return getattr(settings, "ACCOUNT_ALLOW_REGISTRATION", True)

    def get_email_confirmation_url(self, request, emailconfirmation):
        """Constructs the email confirmation (activation) url.

        Note that if you have architected your system such that email
        confirmations are sent outside of the request context `request`
        can be `None` here.
        """
        return f"{FRONTEND_URL}/verify-email/{emailconfirmation.key}"


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request: HttpRequest, sociallogin: Any):
        return getattr(settings, "SOCIALACCOUNT_ALLOW_REGISTRATION", True)

    def pre_social_login(self, request, sociallogin):
        """Add social account to an existing account (if registered with email and password)."""
        social_user = sociallogin.user
        if social_user.id:
            return

        # noinspection PyPep8Naming
        User = get_user_model()  # noqa: N806
        try:
            user = User.objects.get(email=social_user.email)
            sociallogin.state['process'] = 'connect'
            perform_login(request, user, 'none')
        except User.DoesNotExist:
            pass


class GoogleIdentityServiceOAuth2Adapter(GoogleOAuth2Adapter):
    """
    Temporarily override GoogleOauth2Adapter.

    Inherits `GoogleOAuth2Adapter` and works around the key error caused by the
    new response format of Google Identity Service by overriding the `complete_login` method.

    The error is caused by the `response` argument being a string instead of a dict here:
    https://github.com/pennersr/django-allauth/blob/c56e624ffd0be6baf0eac3cd2b0fd4e50929aed3/allauth/socialaccount/providers/google/views.py#L43

    `access_token` is retreived here:
    https://github.com/pennersr/django-allauth/blob/c56e624ffd0be6baf0eac3cd2b0fd4e50929aed3/allauth/socialaccount/providers/oauth2/views.py#L144

    TODO: remove this class when django-allauth is updated to work the
    new response format of Google Identity Service.
    Github issue to track: https://github.com/pennersr/django-allauth/issues/3284
    """

    def complete_login(self, request, app, token, response, **kwargs):  # noqa: ANN001,ANN201,D102,RUF100
        return super().complete_login(request, app, token, {"id_token": response}, **kwargs)
