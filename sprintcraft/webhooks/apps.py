from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class WebhooksConfig(AppConfig):
    name = 'sprintcraft.webhooks'
    verbose_name = _("Webhooks")

    def ready(self):
        try:  # noqa: SIM105
            import sprintcraft.webhooks.signals  # noqa: F401
        except ImportError:
            pass
