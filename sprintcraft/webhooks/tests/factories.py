"""
Fixtures for the webhook module.
"""

from factory import Faker, Sequence
from factory.django import DjangoModelFactory

from sprintcraft.webhooks.models import Webhook, WebhookEvent


class WebhookEventFactory(DjangoModelFactory):
    """
    Factory for webhook events.
    """

    name = Faker('slug')

    class Meta:
        """Config for factory."""

        model = WebhookEvent


class WebhookFactory(DjangoModelFactory):
    """
    Factory for the webhook model.
    """

    payload_url = Sequence(lambda x: f"https://example.com/{x}/")
    active = True

    class Meta:
        """Config for factory."""

        model = Webhook
